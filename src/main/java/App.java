import element.analizer.Analyzer;
import element.helper.HtmlHelper;
import element.searcher.Searcher;

import java.io.File;

public class App {
    public static void main(String[] args) {
        String id = "make-everything-ok-button";
        Analyzer analyzer = new Analyzer();
        Searcher searcher = new Searcher();
        HtmlHelper htmlHelper = new HtmlHelper();

        htmlHelper.findElementById(new File(args[0]), id)
                .ifPresent(analyzer::analyzeElementInfos);

        String cssSelector = searcher.findElement(new File(args[1]), analyzer.getInfos()).cssSelector();

        System.out.println(cssSelector);
    }
}
