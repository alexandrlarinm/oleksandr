package element.service;

import org.jsoup.nodes.Element;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AnalyzerService {

    Map<String, Object> attributes = new HashMap<>();

    public void analyzeElementIndex(Element element) {
        attributes.put("index", element.hasParent() ? element.parents().size() : 0);
    }

    public void analyzeTagName(Element element) {
        attributes.put("tag", element.tagName());
    }

    public void analyzeId(Element element) {
        attributes.put("id", element.hasAttr("id") ? element.id() : "");
    }

    public void analyzeClasses(Element element) {
        attributes.put("classes", element.hasAttr("class") ? element.classNames() : Collections.EMPTY_SET);
    }

    public void analyzeTitle(Element element) {
        attributes.put("title", element.hasAttr("title") ? element.attr("title") : "");
    }

    public void analyzeHref(Element element) {
        attributes.put("href", element.hasAttr("href") ? element.attr("href") : "");
    }

    public void analyzeText(Element element) {
        List<String> list = element.hasText() ? Arrays.asList(element.text().split(" ")) : Collections.EMPTY_LIST;
        attributes.put("text", list);
    }

    static final AnalyzerService getServise() {
        return new AnalyzerService();
    }

    public Map<String, Object> getElementInfos() {
        return attributes;
    }
}
