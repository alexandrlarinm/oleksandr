package element.searcher;

import element.helper.FindHelper;
import element.searcher.paramssearcher.ClassesSearchHandler;
import element.searcher.paramssearcher.Handler;
import element.searcher.paramssearcher.HrefSearchHandler;
import element.searcher.paramssearcher.IdSearchHandler;
import element.searcher.paramssearcher.TagSearchHandler;
import element.searcher.paramssearcher.TextSearchHandler;
import element.searcher.paramssearcher.TitleSearchHandler;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import element.helper.HtmlHelper;

import java.io.File;
import java.util.Map;

public class Searcher {

    private Handler byTag;
    private Handler byId;
    private Handler byClasses;
    private Handler byTitle;
    private Handler byHref;
    private Handler byText;


    private void setUp(FindHelper findHelper) {
        byTag = new TagSearchHandler(findHelper);
        byId = new IdSearchHandler(findHelper);
        byTitle = new TitleSearchHandler(findHelper);
        byHref = new HrefSearchHandler(findHelper);
        byClasses = new ClassesSearchHandler(findHelper);
        byText = new TextSearchHandler(findHelper);

        byTag.setNext(byId);
        byId.setNext(byTitle);
        byTitle.setNext(byHref);
        byHref.setNext(byClasses);
        byClasses.setNext(byText);

    }

    public Element findElement(File htmlFile, Map<String, Object> params) {
        Document document = new HtmlHelper().getDocument(htmlFile);
        FindHelper findHelper = new FindHelper(params);

        setUp(findHelper);
        byTag.handle(document);

        return findHelper.getElements().entrySet()
                .stream().max(Map.Entry.comparingByValue())
                .map(Map.Entry::getKey).orElse(null);
    }
}
