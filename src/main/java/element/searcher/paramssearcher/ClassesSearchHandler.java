package element.searcher.paramssearcher;

import element.helper.FindHelper;
import org.jsoup.nodes.Element;

public class ClassesSearchHandler extends BaseHandler {

    public ClassesSearchHandler(FindHelper helper) {
        super(helper);
    }

    @Override
    public void handle(Element element) {
        helper.findByClasses(element);
        super.handle(element);
    }

}
