package element.searcher.paramssearcher;

import org.jsoup.nodes.Element;

public interface Handler {

    void setNext(Handler handler);

    void handle(Element element);
}
