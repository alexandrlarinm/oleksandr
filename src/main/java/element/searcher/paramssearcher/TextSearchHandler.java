package element.searcher.paramssearcher;

import element.helper.FindHelper;
import org.jsoup.nodes.Element;

public class TextSearchHandler extends BaseHandler {

    public TextSearchHandler(FindHelper helper) {
        super(helper);
    }

    @Override
    public void handle(Element element) {
        helper.findByText(element);
        super.handle(element);
    }

}
