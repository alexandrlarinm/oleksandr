package element.searcher.paramssearcher;

import element.helper.FindHelper;
import org.jsoup.nodes.Element;

public class TagSearchHandler extends BaseHandler {

    public TagSearchHandler(FindHelper helper) {
        super(helper);
    }

    @Override
    public void handle(Element element) {
        helper.findByTag(element);
        super.handle(element);
    }

}
