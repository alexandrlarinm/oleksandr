package element.searcher.paramssearcher;

import element.helper.FindHelper;
import org.jsoup.nodes.Element;

import java.util.Objects;

public abstract class BaseHandler implements Handler {
    protected final FindHelper helper;
    protected Handler next;

    protected BaseHandler(FindHelper helper) {
        this.helper = helper;
    }

    @Override
    public void setNext(Handler handler) {
        next = handler;
    }

    @Override
    public void handle(Element element) {
        if (Objects.nonNull(next)) {
            next.handle(element);
        }
    }

}
