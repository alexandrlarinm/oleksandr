package element.searcher.paramssearcher;

import element.helper.FindHelper;
import org.jsoup.nodes.Element;

public class IdSearchHandler extends BaseHandler {

    public IdSearchHandler(FindHelper helper) {
        super(helper);
    }

    @Override
    public void handle(Element element) {
        helper.findById(element);
        super.handle(element);
    }

}
