package element.searcher.paramssearcher;

import element.helper.FindHelper;
import org.jsoup.nodes.Element;

public class TitleSearchHandler extends BaseHandler {

    public TitleSearchHandler(FindHelper helper) {
        super(helper);
    }

    @Override
    public void handle(Element element) {
        helper.findByTitle(element);
        super.handle(element);
    }

}
