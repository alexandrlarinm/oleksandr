package element.searcher.paramssearcher;

import element.helper.FindHelper;
import org.jsoup.nodes.Element;

public class HrefSearchHandler extends BaseHandler {

    public HrefSearchHandler(FindHelper helper) {
        super(helper);
    }

    @Override
    public void handle(Element element) {
        helper.findByHref(element);
        super.handle(element);
    }

}
