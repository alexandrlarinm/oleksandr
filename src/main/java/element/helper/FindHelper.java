package element.helper;

import org.jsoup.nodes.Element;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FindHelper {

    private final Map<Element, Long> elements = new HashMap<>();
    private final Map<String, Object> params;

    public FindHelper(Map<String, Object> params) {
        this.params = params;
    }

    public void findByIndex(Element element) {
        List<Element> elements = element.getElementsByIndexEquals(1);
    }

    public void findByTag(Element element) {
        List<Element> elements = element.getElementsByTag((String) params.get("tag"));
        elements.forEach(this::addElementToMap);
    }

    private void addElementToMap(Element element) {
        if (elements.containsKey(element)) {
            elements.put(element, elements.get(element) + 1L);
        } else {
            elements.put(element, 1L);
        }
    }

    public Map<Element, Long> getElements() {
        return elements;
    }

    public void findById(Element element) {
        List<Element> elements = element.getElementsByAttributeValue("id", (String) params.get("id"));
        elements.forEach(this::addElementToMap);
    }

    public void findByClasses(Element element) {
        ((Set<String>) params.get("classes")).forEach(className -> {
            List<Element> elements = element.getElementsByClass(className);
            elements.forEach(this::addElementToMap);
        });
    }

    public void findByTitle(Element element) {
        List<Element> elements = element.getElementsByAttributeValue("title", (String) params.get("title"));
        elements.forEach(this::addElementToMap);
    }

    public void findByText(Element element) {
        ((List<String>) params.get("text")).forEach(text -> {
            List<Element> elements = element.getElementsContainingText(text);
            elements.forEach(this::addElementToMap);
        });
    }

    public void findByHref(Element element) {
        List<Element> elements = element.getElementsByAttribute("href");

        elements.stream()
                .filter(this::containsHref)
                .forEach(this::addElementToMap);
    }

    private boolean containsHref(Element element) {
        return element.attr("herf").contains((String) params.get("href"));
    }
}
