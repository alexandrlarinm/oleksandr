package element.helper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

public class HtmlHelper {

    private String charSetName = StandardCharsets.UTF_8.name();

    public Document getDocument(File htmlFile) {
        try {
            return Jsoup.parse(htmlFile, charSetName, htmlFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Optional<Element> findElementById(File htmlFile, String targetElementId) {
        Document doc = getDocument(htmlFile);
        return Optional.ofNullable(doc.getElementById(targetElementId));
    }

    public void setCharSetName(String charSetName) {
        this.charSetName = charSetName;
    }
}
