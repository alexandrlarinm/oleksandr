package element.analizer.handler;

import element.service.AnalyzerService;
import org.jsoup.nodes.Element;

public class TagAnalyzing extends BaseAnalyzer {

    public TagAnalyzing(AnalyzerService service) {
        super(service);
    }

    @Override
    public void handle(Element element) {
        service.analyzeTagName(element);
        super.handle(element);
    }
}
