package element.analizer.handler;

import element.service.AnalyzerService;
import org.jsoup.nodes.Element;

public class HrefAnalyzing extends BaseAnalyzer {

    public HrefAnalyzing(AnalyzerService service) {
        super(service);
    }

    @Override
    public void handle(Element element) {
        service.analyzeHref(element);
        super.handle(element);
    }
}
