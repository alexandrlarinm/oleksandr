package element.analizer.handler;

import org.jsoup.nodes.Element;

import java.util.Objects;

public interface Analyzing {

    void setNext(Analyzing handler);

    void handle(Element element);
}
