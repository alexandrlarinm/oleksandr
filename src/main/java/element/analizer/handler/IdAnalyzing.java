package element.analizer.handler;

import element.service.AnalyzerService;
import org.jsoup.nodes.Element;

public class IdAnalyzing extends BaseAnalyzer {

    public IdAnalyzing(AnalyzerService service) {
        super(service);
    }

    @Override
    public void handle(Element element) {
        service.analyzeId(element);
        super.handle(element);
    }
}
