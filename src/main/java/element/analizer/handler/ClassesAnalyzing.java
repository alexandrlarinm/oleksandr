package element.analizer.handler;

import element.service.AnalyzerService;
import org.jsoup.nodes.Element;

public class ClassesAnalyzing extends BaseAnalyzer {

    public ClassesAnalyzing(AnalyzerService service) {
        super(service);
    }

    @Override
    public void handle(Element element) {
        service.analyzeClasses(element);
        super.handle(element);
    }
}
