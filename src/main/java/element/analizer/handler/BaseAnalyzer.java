package element.analizer.handler;

import element.service.AnalyzerService;
import org.jsoup.nodes.Element;

import java.util.Objects;

public abstract class BaseAnalyzer implements Analyzing {

    protected final AnalyzerService service;
    protected Analyzing next;


    protected BaseAnalyzer(AnalyzerService service) {
        this.service = service;
    }

    @Override
    public void handle(Element element) {
        if (Objects.nonNull(next)) {
            next.handle(element);
        }
    }

    @Override
    public void setNext(Analyzing handler) {
        next = handler;
    }
}
