package element.analizer.handler;

import element.service.AnalyzerService;
import org.jsoup.nodes.Element;

public class TitleAnalyzing extends BaseAnalyzer {

    public TitleAnalyzing(AnalyzerService service) {
        super(service);
    }

    @Override
    public void handle(Element element) {
        service.analyzeTitle(element);
        super.handle(element);
    }
}
