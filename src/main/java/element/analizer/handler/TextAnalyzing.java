package element.analizer.handler;

import element.service.AnalyzerService;
import org.jsoup.nodes.Element;

public class TextAnalyzing extends BaseAnalyzer {

    public TextAnalyzing(AnalyzerService service) {
        super(service);
    }

    @Override
    public void handle(Element element) {
        service.analyzeText(element);
        super.handle(element);
    }
}
