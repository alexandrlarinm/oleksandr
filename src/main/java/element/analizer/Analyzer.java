package element.analizer;

import element.analizer.handler.Analyzing;
import element.analizer.handler.ClassesAnalyzing;
import element.analizer.handler.HrefAnalyzing;
import element.analizer.handler.IdAnalyzing;
import element.analizer.handler.TagAnalyzing;
import element.analizer.handler.TextAnalyzing;
import element.analizer.handler.TitleAnalyzing;
import element.service.AnalyzerService;
import org.jsoup.nodes.Element;

import java.util.Map;

public class Analyzer {

    private AnalyzerService service = new AnalyzerService();
    private Analyzing tagAnalyzing = new TagAnalyzing(service);
    private Analyzing idAnalyzing = new IdAnalyzing(service);
    private Analyzing classesAnalyzing = new ClassesAnalyzing(service);
    private Analyzing titleAnalyzing = new TitleAnalyzing(service);
    private Analyzing hrefAnalyzing = new HrefAnalyzing(service);
    private Analyzing textAnalyzing = new TextAnalyzing(service);

    private void setUpAnalyzer() {
        idAnalyzing.setNext(tagAnalyzing);
        tagAnalyzing.setNext(classesAnalyzing);
        classesAnalyzing.setNext(titleAnalyzing);
        titleAnalyzing.setNext(hrefAnalyzing);
        hrefAnalyzing.setNext(textAnalyzing);
    }

    public Analyzer() {
        setUpAnalyzer();
    }

    public void analyzeElementInfos(Element element) {
        idAnalyzing.handle(element);
    }

    public Map<String, Object> getInfos() {
        return service.getElementInfos();
    }

}
